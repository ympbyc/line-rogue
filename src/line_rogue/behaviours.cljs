(ns behaviours.core)

(defn behaviour [] (atom nil)) ;;behaviourはリストのatom

(defn behaviour-cons! [x beh] ;;behaviourに値を追加する. atomの中身を差し替える.
  (swap! beh (partial cons x)))

(defn behaviour-map
  "新しいbehaviour`dst-beh`を作って、`beh`に値xが追加される度に`dst-beh`にxに`f`を適用した値を追加する"
  [f beh]
  (let [dst-beh (behaviour)]
    (add-watch beh (gensym)
               (fn [_ _ _ [new-head & _]]
                 (behaviour-cons! (f new-head) dst-beh)))
    dst-beh))

(defn behaviour-filter
  "新しいbehaviour、`dst-beh`を作り、`beh`に値が入る度に`f`を通して真だった場合にのみ`dst-beh`に追加する"
  [f beh]
  (let [dst-beh (behaviour)]
    (add-watch beh (gensym)
               (fn [_ _ _ [new-head & _]]
                 (when (f new-head)
                   (behaviour-cons! new-head dst-beh))))
    dst-beh))

(defn behaviour-inc
  "behaviourの最新の値をincしてbehaviourに追加する"
  [beh]
  (behaviour-cons! (->> beh deref first inc) beh))

(defn behaviour-dec
  "behaviourの最新の値をdecしてbehaviourに追加する"
  [beh]
  (behaviour-cons! (->> beh deref first dec) beh))
