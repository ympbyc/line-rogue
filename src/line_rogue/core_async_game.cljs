(ns line-rogue.core-async
  (:use [cljs.core.async :only [chan <! put!]]
        [line-rogue.base :only [left-key right-key render-game]])
  (:use-macros [cljs.core.async.macros :only [go]]))

;;入力されたキーコードが流れ込んでくるチャンネル
(def input-chan
  (let [c (chan)]
    (.addEventListener js/document "keydown" #(put! c (.-keyCode %)))
    c))


(defn next-x
  "現在のx座標と入力から次のx座標を得る"
  [current-x key-code]
  (condp =  key-code
    left-key  (dec current-x)
    right-key (inc current-x)
    current-x))


;;;;;;;;;;;;;;;;;;;;;
;;;; ここに注目! ;;;;
;;;;;;;;;;;;;;;;;;;;;
(defn main []
  (go
   (loop [player-x 5]
     (let [key-code (<! input-chan)                ;;R: 入力があるまでブロックされる(ように見える)
           x        (next-x player-x key-code)]    ;;E: 次の位置を計算
       (render-game x "core-async")                ;;P: 描画
       (recur x)))))                               ;;L: 再帰

(main)
