(ns line-rogue.base)

(def left-key 72)     ;;Hキーのキーコード
(def right-key 76)    ;;Lキーのキーコード
(def board-width 30)  ;;盤の横幅

(def by-id
  (memoize
   (fn [id] (.getElementById js/document id))))

(defn render-game
  "プレイヤーの座標と描画領域のidをとって盤を描画する"
  [player-x dom-id]
  (->> (for [x (range board-width)]
         (if (= x player-x) "@" "."))
       (apply str ,,,)
       (set! (.-innerHTML (by-id dom-id)) ,,,)))
