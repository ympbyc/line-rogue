(ns line-rogue.frp
  (:require [behaviours.core :as b])
  (:use [line-rogue.base :only [left-key right-key render-game]]))

(defn render [player-x]
  (render-game player-x "frp"))

;;プレイヤー座標の垂れ流し
(def player-x-beh (b/behaviour))
(b/behaviour-cons! 5 player-x-beh) ;;init value


(defn dom-events
  "`el`で発火した`ev`のbehaviour"
  [el ev]
  (let [beh (b/behaviour)]
    (.addEventListener el ev #(b/behaviour-cons! % beh))
    beh))


;;;キーコードの垂れ流し
(def key-code-beh
  (->> (dom-events js/document "keydown")      ;;keydownのイベントオブジェクトのbehaviour
       (b/behaviour-map #(->> % .-keyCode))))  ;;keyCodeのbehaviour


;;;左に移動
(->> key-code-beh
     (b/behaviour-filter #(= left-key %))
     (b/behaviour-map    #(b/behaviour-dec player-x-beh)))


;;;右に移動
(->> key-code-beh
     (b/behaviour-filter #(= right-key %))
     (b/behaviour-map    #(b/behaviour-inc player-x-beh)))


;;;描画
(b/behaviour-map render player-x-beh)
