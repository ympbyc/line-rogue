(ns line-rogue.event-driven
  (:use [line-rogue.base :only [left-key right-key render-game]]))


;;プレイヤー座標をatomで管理. イケてない点
(def player-x (atom 5))

(defn render [player-x]
  (render-game player-x "event-driven"))

(defn game-turn
  "ターン"
  [key]
  (condp = key
    left-key
    (swap! player-x dec)

    right-key
    (swap! player-x inc)

    nil)
  (render @player-x))

(.addEventListener js/document "keydown" #(game-turn (.-keyCode %)))
