# line-rogue

1行ローグライク.
イベント駆動, FRP, そしてcore.asyncを比較するための死ぬほどつまらないゲームです。

## Installation

```
git clone https://ympbyc@bitbucket.org/ympbyc/line-rogue.git
cd line-rogue
lein cljsbuild once
```

## Usage

index.htmlをお気に入りのブラウザで開いてください

## Article

docs/article.md

## License

Copyright © 2013 Minori Yamashita

Distributed under the Eclipse Public License, the same as Clojure.
